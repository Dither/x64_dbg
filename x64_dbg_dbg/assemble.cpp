#include "assemble.h"

static bool cbUnknown(const char* text, ULONGLONG* value)
{
    if(!text || !value)
        return false;
    duint val;
    if(!valfromstring(text, &val))
        return false;
    *value = val;
    return true;
}

static void replace(std::string & src, const std::string & from, const std::string & to)
{
    if(from.empty())
        return;
    std::string::size_type start_pos = 0;
    while((start_pos = src.find(from, start_pos)) != std::string::npos)
    {
        src.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
}

bool assemble(duint addr, unsigned char* dest, int* size, const char* code, char* error)
{
    if(!code)
        return false;

    std::stringstream instructions(code);
    std::string line;

    instructions.seekp(0, std::ios::end);
    std::stringstream::pos_type length = instructions.tellp();
    instructions.seekp(0, std::ios::beg);

    XEDPARSE parse;
    uint16_t instr_len = 0;

    if(length > 0)
    {
        while(std::getline(instructions, line, ASSEMBLY_LINE_DELIMITER))
        {
            if(line.length() >= XEDPARSE_MAXBUFSIZE)
                continue;

            memset(&parse, 0, sizeof(parse));
#ifdef _WIN64
            parse.x64 = true;
#else //x86
            parse.x64 = false;
#endif
            parse.cbUnknown = cbUnknown;
            parse.cip = addr;

            strcpy(parse.instr, line.c_str());
            if(XEDParseAssemble(&parse) == XEDPARSE_ERROR)
            {
                if(error)
                    strcpy(error, parse.error);
                return false;
            }

            if(dest)
                memcpy(dest + instr_len, parse.dest, parse.dest_size);
            instr_len += parse.dest_size;

            if(instr_len >= MAX_ASSEMBLE_SIZE)
                return false;
        }
    }
    else
        return false;

    if(size)
        *size = instr_len;

    return 0 < instr_len;
}

bool assembleat(duint addr, const char* code, int* size, char* error, bool fillnop)
{
    int new_instr_len;
    unsigned char instr_cache[MAX_ASSEMBLE_SIZE];

    if(!assemble(addr, instr_cache, &new_instr_len, code, error))
        return false;

    //calculate the number of NOPs to insert
    int old_instr_len = disasmgetsize(addr);
    while(old_instr_len < new_instr_len)
        old_instr_len += disasmgetsize(addr + old_instr_len);

    int nop_size = old_instr_len - new_instr_len;
    unsigned char nops[MAX_ASSEMBLE_SIZE];
    memset(nops, 0x90, sizeof(nops));

    if(size)
        *size = new_instr_len;

    bool ret = mempatch(fdProcessInfo->hProcess, (void*)addr, instr_cache, new_instr_len, 0);
    if(ret && fillnop && nop_size)
    {
        if(!mempatch(fdProcessInfo->hProcess, (void*)(addr + new_instr_len), nops, nop_size, 0))
            ret = 0;
        else
            new_instr_len += nop_size;

        if(size)
            *size += nop_size;
    }

    if(ret)
    {
        GuiUpdatePatches();
    }
    else
        return false;

    return 0 < new_instr_len;
}
