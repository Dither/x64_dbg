#ifndef _ASSEMBLE_H
#define _ASSEMBLE_H

#include "_global.h"
#include "memory.h"
#include "debugger.h"
#include "XEDParse\XEDParse.h"
#include "value.h"
#include "disasm_helper.h"
#include "console.h"
#include <sstream>

#define MAX_ASSEMBLE_SIZE 1024
#define ASSEMBLY_LINE_DELIMITER ';'

bool assemble(duint addr, unsigned char* dest, int* size, const char* code, char* error);
bool assembleat(duint addr, const char* code, int* size, char* error, bool fillnop);

#endif // _ASSEMBLE_H
