#include "AssemblyDialog.h"
#include "ui_AssemblyDialog.h"
#include "CPUDisassembly.h"

AssemblyDialog::AssemblyDialog(QWidget* parent) : QDialog(parent), ui(new Ui::AssemblyDialog)
{
    ui->setupUi(this);
    //setModal(true);
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
    setWindowFlags(Qt::Dialog | Qt::WindowSystemMenuHint | Qt::WindowTitleHint | Qt::MSWindowsFixedSizeDialogHint);
#endif
    setFixedSize(this->size()); //fixed size
    setModal(true); //modal window
}

AssemblyDialog::~AssemblyDialog()
{
    delete ui;
}

void AssemblyDialog::setCode(QString txt)
{
    ui->codeEdit->setPlainText(txt);
    ui->codeEdit->selectAll();
}

void AssemblyDialog::setPreview(QString txt)
{
    ui->codePreview->setPlainText(txt);
    //ui->codePreview->selectAll();
}


void AssemblyDialog::enableAsmButton(bool bEnable)
{
    ui->assembleCode->setEnabled(bEnable);
}

void AssemblyDialog::setCheckBoxText(const QString & text)
{
    ui->fillWithNops->setText(text);
}

void AssemblyDialog::setCheckBox(bool bSet)
{
    ui->fillWithNops->setChecked(bSet);
    bChecked = bSet;
}

void AssemblyDialog::enableCheckBox(bool bEnable)
{
    if(bEnable)
        ui->fillWithNops->show();
    else
        ui->fillWithNops->hide();
}


void AssemblyDialog::on_codeEdit_textChanged()
{
    codeTextData = ui->codeEdit->toPlainText();
    emit updated(this);
}

void AssemblyDialog::on_assembleCode_clicked()
{
    done(QDialog::Accepted);
}

void AssemblyDialog::on_assembleCancel_clicked()
{
    done(QDialog::Rejected);
}

void AssemblyDialog::on_fillWithNops_toggled(bool checked)
{
    bChecked = checked;
}
