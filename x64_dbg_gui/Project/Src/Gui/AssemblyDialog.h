#ifndef ASSEMBLYDIALOG_H
#define ASSEMBLYDIALOG_H

#include <QDialog>

namespace Ui
{
class AssemblyDialog;
}

class AssemblyDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AssemblyDialog(QWidget* parent = 0);
    ~AssemblyDialog();

    QString codeTextData, previewData;
    int dwStatus;
    bool bChecked;

    void setCode(QString txt);
    void setPreview(QString txt);
    void enableAsmButton(bool bEnable = true);
    void enableCheckBox(bool bEnable = true);
    void setCheckBox(bool bSet = true);
    void setCheckBoxText(const QString & text);

signals:
    void updated(QWidget* wdg);

private slots:
    void on_codeEdit_textChanged();
    void on_assembleCode_clicked();
    void on_assembleCancel_clicked();
    void on_fillWithNops_toggled(bool checked);

public:
    Ui::AssemblyDialog* ui;
};

#endif // ASSEMBLYDIALOG_H

